
function FizzBuzz(){
    for(let i=1; i <= 100; i++){
        console.log(FizzBuzzValue(i))
    }
}

// FizzBuzzValue
// @param i Number - the FizzBuzz input parameter
// returns String - FizzBuzz value
function FizzBuzzValue(i){
    let returnValue = ""

    if(i%3 === 0){
        returnValue += "Fizz"
    }
    if(i%5 === 0){
        returnValue += "Buzz"
    }
    if(returnValue === ""){
        returnValue += i // ""+Number = string
    }

    return returnValue
}

module.exports.FizzBuzz = FizzBuzz
module.exports.FizzBuzzValue = FizzBuzzValue

//if not imported = launched directy -> run FizzBuzz
if (require.main === module) {
    FizzBuzz();
}
