
let FizzBuzzValue = require('./fizzbuzz.js').FizzBuzzValue

test('a. čísla dělitelná 3-mi a 5-ti vypíšou text ‘FizzBuzz’', ()=>{
    for(let i=1; i <= 100; i++){
        if(i%15 === 0){
            expect(FizzBuzzValue(i)).toBe("FizzBuzz")
        }
    }
})

test('b. čísla dělitelná 3-mi vypíšou text ‘Fizz’', ()=>{
    for(let i=1; i <= 100; i++){
        if(i%3 === 0 && i%15 !== 0){
            expect(FizzBuzzValue(i)).toBe("Fizz")
        }
    }
})

test('c. čísla dělitelná 5-ti vypíšou text ‘Buzz’', ()=>{
    for(let i=1; i <= 100; i++){
        if(i%5 === 0 && i%15 !== 0){
            expect(FizzBuzzValue(i)).toBe("Buzz")
        }
    }
})

test('d. čísla, která nesplňují žádnou z podmínek vypíšou pouze číslo', ()=>{
    for(let i=1; i <= 100; i++){
        if(i%5 !== 0 && i%3 !== 0){
            expect(FizzBuzzValue(i)).toBe(i.toString())
        }
    }
})
